package pl.hunter71.websiterankchecker.utils

import retrofit2.Call

inline fun <reified T> Call<T>.url(): String = request().url().toString()
