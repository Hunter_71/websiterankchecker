package pl.hunter71.websiterankchecker.cli

import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.core.subcommands
import com.github.ajalt.clikt.parameters.options.versionOption
import pl.hunter71.websiterankchecker.api.WebsiteRankCheckerAPI


object WebsiteRankCheckerCLI: CliktCommand(name = "website-rank-checker") {
    override fun run() {}

    fun runCLI(args: Array<String>) = versionOption(WebsiteRankCheckerAPI.version())
        .subcommands(
            Rank,
            Search,
            TopResults
        )
        .main(args)
}
