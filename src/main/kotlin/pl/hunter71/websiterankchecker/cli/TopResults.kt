package pl.hunter71.websiterankchecker.cli

import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.parameters.arguments.argument
import com.github.ajalt.clikt.parameters.arguments.multiple
import com.github.ajalt.clikt.parameters.options.default
import com.github.ajalt.clikt.parameters.options.option
import com.github.ajalt.clikt.parameters.types.int
import com.github.ajalt.clikt.parameters.types.restrictTo
import pl.hunter71.websiterankchecker.api.WebsiteRankCheckerAPI
import kotlin.system.exitProcess

private val resultsLimit = WebsiteRankCheckerAPI.googleResultsLimit()


object TopResults: CliktCommand(
    name = "top",
    help = "Return up to $resultsLimit top search results for given phrase"
) {
    private val limit: Int
        by option("-n", "--limit", help = "Limit results to given number, max $resultsLimit")
            .int()
            .restrictTo(1..resultsLimit, clamp = true)
            .default(resultsLimit)

    private val tokens: List<String>
        by argument(name = "phrase", help = "Search phrase")
            .multiple(required = true)

    override fun run() {
        val phrase = tokens.joinToString(" ")
        val result = WebsiteRankCheckerAPI.googleTopResults(phrase, limit)

        echo("Top results for '$phrase':\n$result\n")

        // Program does not exit for a long time after processing,
        // although result are echo'ed correctly within seconds.
        // Thus process is directly set to exit right after processing.
        exitProcess(0)
    }
}
