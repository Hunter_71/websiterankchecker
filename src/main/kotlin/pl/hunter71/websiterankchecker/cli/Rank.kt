package pl.hunter71.websiterankchecker.cli

import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.core.PrintMessage
import com.github.ajalt.clikt.parameters.arguments.argument
import com.github.ajalt.clikt.parameters.arguments.multiple
import pl.hunter71.websiterankchecker.api.WebsiteRankCheckerAPI
import kotlin.system.exitProcess


object Rank: CliktCommand(
    name = "rank",
    help = "Return rank of given website for given phrase"
) {
    private val maxResults = WebsiteRankCheckerAPI.googleMaxResults()

    private val website: String
        by argument(name = "website", help = "Website to be ranked, ex. parafiadzialoszyn.pl")

    private val tokens: List<String>
        by argument(name = "phrase", help = "Search phrase")
            .multiple(required = true)

    override fun run() {
        val phrase = tokens.joinToString(" ")
        val result = WebsiteRankCheckerAPI.googleRank(phrase, website)

        if (result == null) {
            throw PrintMessage(
                "Website $website was not found in first $maxResults results."
            )
        } else {
            echo(result.toStringRanked())
        }

        // Program does not exit for a long time after processing,
        // although result are echo'ed correctly within seconds.
        // Thus process is directly set to exit right after processing.
        exitProcess(0)
    }
}
