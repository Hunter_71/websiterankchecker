package pl.hunter71.websiterankchecker.api

import pl.hunter71.websiterankchecker.search.service.GoogleSearchAPIService
import pl.hunter71.websiterankchecker.search.service.SearchService
import pl.hunter71.websiterankchecker.search.service.results.SearchServiceResultItem
import pl.hunter71.websiterankchecker.search.service.results.SearchServiceResults


object WebsiteRankCheckerAPI {
    private const val VERSION = "0.0.1"

    fun version(): String = VERSION
    fun googleResultsLimit(): Int = SearchService.RESULT_CHUNK
    fun googleMaxResults(): Int = SearchService.MAX_RESULTS
    fun googleMaxPages(): Int = SearchService.MAX_PAGE_NUMBER

    fun googleTopResults(phrase: String, limit: Int = googleResultsLimit()): SearchServiceResults =
        googleResults(phrase, limit = limit)

    fun googleResults(phrase: String, page: Int = 1, limit: Int = googleResultsLimit()): SearchServiceResults =
        GoogleSearchAPIService.searchResults(phrase, page, limit)

    fun googleRank(phrase: String, website: String): SearchServiceResultItem? =
        GoogleSearchAPIService.rank(phrase, website)
}
