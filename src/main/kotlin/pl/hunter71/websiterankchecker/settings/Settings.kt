package pl.hunter71.websiterankchecker.settings

import com.natpryce.konfig.ConfigurationProperties
import com.natpryce.konfig.PropertyGroup
import com.natpryce.konfig.getValue
import com.natpryce.konfig.stringType


object Settings {
    object Global {
        val loggingLevel = config[global.logging_level]
    }

    object GoogleSearch {
        val apiKey = config[google_search.api_key]
        val searchEngineId = config[google_search.search_engine_id]
        val loggingLevel = config[google_search.logging_level]
    }

    private val config = ConfigurationProperties.fromLocalResource(
        localResource = "application.local.ini",
        fallback = "application.ini"
    )
}

object global: PropertyGroup() {
    val logging_level by stringType
}

object google_search: PropertyGroup() {
    val api_key by stringType
    val search_engine_id by stringType
    val logging_level by stringType
}
