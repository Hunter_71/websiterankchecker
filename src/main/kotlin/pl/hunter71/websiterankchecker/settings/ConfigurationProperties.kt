package pl.hunter71.websiterankchecker.settings

import com.natpryce.konfig.ConfigurationProperties
import com.natpryce.konfig.Misconfiguration


fun ConfigurationProperties.Companion.fromLocalResource(
    localResource: String,
    fallback: String
): ConfigurationProperties {

    return try {
        fromResource(localResource)
    } catch (e: Misconfiguration) {
        fromResource(fallback)
    }
}
