@file:JvmName("Main")

package pl.hunter71.websiterankchecker

import pl.hunter71.websiterankchecker.cli.WebsiteRankCheckerCLI


fun main(args: Array<String>) {
    WebsiteRankCheckerCLI.runCLI(args)
}
