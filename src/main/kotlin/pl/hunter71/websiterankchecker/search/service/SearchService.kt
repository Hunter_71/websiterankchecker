package pl.hunter71.websiterankchecker.search.service

import pl.hunter71.websiterankchecker.search.service.results.SearchServiceResultItem
import pl.hunter71.websiterankchecker.search.service.results.SearchServiceResults


interface SearchService {
    companion object {
        const val RESULT_CHUNK = 10
        const val MAX_RESULTS = 100
        const val MAX_PAGE_NUMBER = MAX_RESULTS / RESULT_CHUNK
    }

    fun searchResults(searchPhrase: String, page: Int = 1, limit: Int = RESULT_CHUNK): SearchServiceResults
    fun rank(searchPhrase: String, website: String): SearchServiceResultItem?
}
