package pl.hunter71.websiterankchecker.search.service

import ch.qos.logback.classic.Level
import ch.qos.logback.classic.Logger
import mu.KotlinLogging
import pl.hunter71.websiterankchecker.search.service.results.SearchServiceResultItem
import pl.hunter71.websiterankchecker.search.service.results.SearchServiceResults
import pl.hunter71.websiterankchecker.settings.Settings
import retrofit2.Response


abstract class AbstractSearchService(
    loggingLevel: String = Settings.Global.loggingLevel
): SearchService {
    protected val logger = KotlinLogging.logger(this.javaClass.name)
    protected val emptyResult = SearchServiceResults()

    init {
        setLogger(loggingLevel)
    }

    override fun rank(searchPhrase: String, website: String): SearchServiceResultItem? {
        var firstOccurrence: SearchServiceResultItem? = null
        var page = 1

        while (firstOccurrence == null && page <= SearchService.MAX_PAGE_NUMBER) {
            val response = searchResults(searchPhrase, page)
            firstOccurrence = response.items.firstOrNull { sanitise(website) in it.url }

            page++
        }

        return firstOccurrence
    }

    private fun sanitise(website: String): String = website
        .removePrefix("https://")
        .removePrefix("http://")
        .removePrefix("www.")
        .replaceAfter("/", "")
        .removeSuffix("/")

    protected fun reportFailedResponse(response: Response<*>) {
        logger.error("${response.code()}: ${response.message()}")
    }

    protected fun reportUnavailablePage(page: Int) {
        logger.debug("Page $page is unavailable")
    }

    private fun setLogger(loggingLevel: String) {
        (logger.underlyingLogger as Logger).level = Level.toLevel(loggingLevel)
    }
}
