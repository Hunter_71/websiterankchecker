package pl.hunter71.websiterankchecker.search.service.results


interface SearchServiceResultsConvertible {
    fun toSearchServiceResult(page: Int): SearchServiceResults
}
