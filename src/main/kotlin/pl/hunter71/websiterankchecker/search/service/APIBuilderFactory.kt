package pl.hunter71.websiterankchecker.search.service

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


object APIBuilderFactory {
    inline fun <reified T> create(url: String): T = Retrofit.Builder()
        .baseUrl(url)
        .addConverterFactory(GsonConverterFactory.create())
        .build()
        .create(T::class.java)
}
