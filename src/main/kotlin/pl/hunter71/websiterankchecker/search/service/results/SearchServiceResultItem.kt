package pl.hunter71.websiterankchecker.search.service.results

import pl.hunter71.websiterankchecker.search.service.SearchService


data class SearchServiceResultItem(
    val title: String,
    val url: String,
    val page: Int,
    val position: Int
) {
    val rank = (page - 1) * SearchService.RESULT_CHUNK + position

    override fun toString(): String {
        return "$title --> $url"
    }

    fun toStringRanked(pad: Int = 4): String {
        val formattedRank = rank.toString().padStart(pad, ' ')
        val formattedItem = "$title\n${" ".repeat(pad + 2)}$url"
        return "$formattedRank. $formattedItem"
    }
}
