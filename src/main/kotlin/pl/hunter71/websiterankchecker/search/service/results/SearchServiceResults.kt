package pl.hunter71.websiterankchecker.search.service.results


data class SearchServiceResults(
    val items: List<SearchServiceResultItem> = emptyList()
) {
    override fun toString(): String {
        if (items.isEmpty()) {
            return "<empty>"
        }

        val formattedPage = "Page ${items[0].page}:"
        val formattedItems = items.joinToString("\n") {
            it.toStringRanked()
        }
        return "$formattedPage\n$formattedItems"
    }
}
