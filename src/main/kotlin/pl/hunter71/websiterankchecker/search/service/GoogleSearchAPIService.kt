package pl.hunter71.websiterankchecker.search.service

import pl.hunter71.websiterankchecker.search.google.GoogleSearchAPI
import pl.hunter71.websiterankchecker.search.google.GoogleSearchResults
import pl.hunter71.websiterankchecker.search.service.results.SearchServiceResults
import pl.hunter71.websiterankchecker.settings.Settings
import pl.hunter71.websiterankchecker.utils.url
import retrofit2.Call

val settings = Settings.GoogleSearch


object GoogleSearchAPIService: AbstractSearchService(
    loggingLevel = settings.loggingLevel
) {
    private const val GOOGLE_SEARCH_API_URL = "https://www.googleapis.com/customsearch/"

    private val service: GoogleSearchAPI = APIBuilderFactory.create(GOOGLE_SEARCH_API_URL)
    private val authorizationParams = mapOf(
        "cx" to settings.searchEngineId,
        "key" to settings.apiKey
    )

    override fun searchResults(searchPhrase: String, page: Int, limit: Int): SearchServiceResults {
        logger.debug("Search for page $page of Google Search API results for phrase: '$searchPhrase'")

        require(page in 1..SearchService.MAX_PAGE_NUMBER) {
            reportUnavailablePage(page)
            return emptyResult
        }

        val request = createRequest(searchPhrase, page, limit)
        return retrieveResponse(request, page)
    }

    private fun createRequest(searchPhrase: String, page: Int, limit: Int): Call<GoogleSearchResults> {
        val params = authorizationParams + mapOf(
            "q" to searchPhrase,
            "fields" to GoogleSearchResults.mapToResponseFields(),
            "start" to start(page),
            "num" to limit.toString()
        )
        val call = service.searchResults(params)

        logger.debug("GET: ${call.url()}")

        return call
    }

    private fun retrieveResponse(request: Call<GoogleSearchResults>, page: Int): SearchServiceResults {
        val response = request.execute()

        logger.debug("Response: ${response.body()}")

        if (!response.isSuccessful) {
            reportFailedResponse(response)
        }

        if (response.body()?.items == null) {
            reportUnavailablePage(page)
        }

        return response.body()?.toSearchServiceResult(page) ?: emptyResult
    }

    private fun start(page: Int): String {
        val startIndex = (page - 1) * SearchService.RESULT_CHUNK + 1
        return startIndex.toString()
    }
}
