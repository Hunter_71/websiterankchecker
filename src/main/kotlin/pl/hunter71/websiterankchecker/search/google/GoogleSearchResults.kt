package pl.hunter71.websiterankchecker.search.google

import pl.hunter71.websiterankchecker.search.service.results.SearchServiceResultItem
import pl.hunter71.websiterankchecker.search.service.results.SearchServiceResults
import pl.hunter71.websiterankchecker.search.service.results.SearchServiceResultsConvertible


data class GoogleSearchResults(val items: List<GoogleSearchResultMetaItem>?): SearchServiceResultsConvertible {
    companion object: ResponseFieldsMapped {
        override fun mapToResponseFields(): String {
            return this::class.java.enclosingClass.declaredFields
                .filterNot { it.name == "Companion" }
                .filterNot { it.name.startsWith("$") }
                .joinToString(",") {
                    "${it.name}(${GoogleSearchResultMetaItem.mapToResponseFields()})"
                }
        }
    }

    override fun toSearchServiceResult(page: Int): SearchServiceResults = SearchServiceResults(
        items = items?.map {
            SearchServiceResultItem(
                title = it.title,
                url = it.link,
                page = page,
                position = items.indexOf(it) + 1
            )
        } ?: emptyList()
    )
}
