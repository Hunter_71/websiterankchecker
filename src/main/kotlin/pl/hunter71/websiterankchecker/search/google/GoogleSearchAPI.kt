package pl.hunter71.websiterankchecker.search.google

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.QueryMap


interface GoogleSearchAPI {
    @GET("v1")
    fun searchResults(@QueryMap(encoded = true) params: Map<String, String>): Call<GoogleSearchResults>
}
