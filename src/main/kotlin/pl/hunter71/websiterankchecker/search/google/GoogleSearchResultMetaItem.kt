package pl.hunter71.websiterankchecker.search.google


data class GoogleSearchResultMetaItem(val title: String, val link: String, val formattedUrl: String) {
    companion object: ResponseFieldsMapped {
        override fun mapToResponseFields(): String {
            return this::class.java.enclosingClass.declaredFields
                .filterNot { it.name == "Companion" }
                .filterNot { it.name.startsWith("$") }
                .joinToString(",") {
                    "${it.name}"
                }
        }
    }
}
