package pl.hunter71.websiterankchecker.api

import io.mockk.every
import io.mockk.mockkObject
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNull
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import pl.hunter71.websiterankchecker.search.service.GoogleSearchAPIService
import pl.hunter71.websiterankchecker.search.service.SearchService
import pl.hunter71.websiterankchecker.search.service.results.SearchServiceResultItem
import pl.hunter71.websiterankchecker.search.service.results.SearchServiceResults


internal class WebsiteRankCheckerAPITest {
    private val results = SearchServiceResults(
        items = listOf(
            SearchServiceResultItem("Google", "google.com", 1, 1),
            SearchServiceResultItem("Google Search", "google.com/search", 1, 2),
            SearchServiceResultItem("Bing", "bing.com", 2, 3)
        )
    )

    @BeforeEach
    fun `set up`() {
        mockGoogleSearchService()
    }

    @Test
    fun `check API version`() {
        assertEquals("0.0.1", WebsiteRankCheckerAPI.version())
    }

    @Test
    fun `check Google results invalid page`() {
        val result = WebsiteRankCheckerAPI.googleResults("search engine", -1)
        assertEquals(SearchServiceResults(), result)
        assertEquals(0, result.items.size)
    }

    @Test
    fun `check Google result out of scope page`() {
        val result = WebsiteRankCheckerAPI.googleResults("search engine", SearchService.MAX_PAGE_NUMBER + 1)
        assertEquals(SearchServiceResults(), result)
        assertEquals(0, result.items.size)
    }

    @Test
    fun `check Google result by pages`() {
        val page1Results = WebsiteRankCheckerAPI.googleResults("search engine", 1)
        assertEquals(SearchServiceResults(items = results.items.filter { it.page == 1 }), page1Results)
        assertEquals(2, page1Results.items.size)

        val page2Results = WebsiteRankCheckerAPI.googleResults("search engine", 2)
        assertEquals(SearchServiceResults(items = results.items.filter { it.page == 2 }), page2Results)
        assertEquals(1, page2Results.items.size)

        val page3Results = WebsiteRankCheckerAPI.googleResults("search engine", 3)
        assertEquals(SearchServiceResults(), page3Results)
        assertEquals(0, page3Results.items.size)
    }

    @Test
    fun `check Google top results`() {
        val topResults = WebsiteRankCheckerAPI.googleResults("search engine", 1)
        assertEquals(SearchServiceResults(items = results.items.filter { it.page == 1 }), topResults)
        assertEquals(2, topResults.items.size)
    }

    @Test
    fun `check Google top results match Google results first page`() {
        assertEquals(
            WebsiteRankCheckerAPI.googleTopResults("search engine"),
            WebsiteRankCheckerAPI.googleResults("search engine", 1)
        )
    }

    @Test
    fun `check Google ranks`() {
        assertEquals(
            results.items[0],
            WebsiteRankCheckerAPI.googleRank("search engine", "google.com")
        )

        assertEquals(
            results.items[1],
            WebsiteRankCheckerAPI.googleRank("search engine", "bing.com")
        )

        assertNull(WebsiteRankCheckerAPI.googleRank("search engine", "yahoo.com"))
    }

    private fun mockGoogleSearchService() {
        mockkObject(GoogleSearchAPIService)

        every {
            GoogleSearchAPIService.rank("search engine", "google.com")
        } returns results.items[0]

        every {
            GoogleSearchAPIService.rank("search engine", "bing.com")
        } returns results.items[1]

        every {
            GoogleSearchAPIService.rank("search engine", "yahoo.com")
        } returns null

        for (page in 1..3) {
            every {
                GoogleSearchAPIService.searchResults("search engine", page = page)
            } returns SearchServiceResults(items = results.items.filter { it.page == page })
        }
    }
}
