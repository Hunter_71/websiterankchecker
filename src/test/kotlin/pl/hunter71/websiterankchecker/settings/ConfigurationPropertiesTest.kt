package pl.hunter71.websiterankchecker.settings

import com.natpryce.konfig.ConfigurationProperties
import io.mockk.mockkObject
import io.mockk.verify
import org.junit.jupiter.api.Test


internal class ConfigurationPropertiesTest {
    private val localProperties = "application.test.ini"

    @Test
    fun `test fromLocalResource use fallback when local properties does not exists`() {
        mockkObject(ConfigurationProperties)

        ConfigurationProperties.fromLocalResource("fake", fallback = localProperties)

        verify(exactly = 1) { ConfigurationProperties.fromResource("fake") }
        verify(exactly = 1) { ConfigurationProperties.fromResource(localProperties) }
    }

    @Test
    fun `test fromLocalResource read from existing local properties`() {
        mockkObject(ConfigurationProperties)

        ConfigurationProperties.fromLocalResource(localProperties, fallback = "fallback")

        verify(exactly = 1) { ConfigurationProperties.fromResource(localProperties) }
        verify(exactly = 0) { ConfigurationProperties.fromResource("fallback") }
    }
}
