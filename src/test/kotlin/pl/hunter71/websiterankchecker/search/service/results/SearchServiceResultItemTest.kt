package pl.hunter71.websiterankchecker.search.service.results

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test


internal class SearchServiceResultItemTest {
    private val title = "Example title 1"
    private val url = "http://www.example.com"
    private val position = 3
    private val page = 4
    private val result = SearchServiceResultItem(title, url, page, position)

    @Test
    fun `check result item`() {
        assertEquals(title, result.title)
        assertEquals(url, result.url)
        assertEquals(position, result.position)
        assertEquals(page, result.page)
        assertEquals(33, result.rank)
    }

    @Test
    fun `check toString for result item`() {
        assertEquals("$title --> $url", result.toString())
    }

    @Test
    fun `check toStringRanked for result item`() {
        val expected = "  33. $title\n      $url"
        assertEquals(expected, result.toStringRanked())
    }
}
