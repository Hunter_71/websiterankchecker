package pl.hunter71.websiterankchecker.search.service

import ch.qos.logback.classic.Logger
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNull
import org.junit.jupiter.api.Test
import pl.hunter71.websiterankchecker.search.service.results.SearchServiceResultItem
import pl.hunter71.websiterankchecker.search.service.results.SearchServiceResults


internal class AbstractSearchServiceTest {

    @Test
    fun `check for default logging level`() {
        class FakeService: AbstractSearchService() {
            override fun searchResults(searchPhrase: String, page: Int, limit: Int) = SearchServiceResults()

            fun loggerLevel(): String {
                return (logger.underlyingLogger as Logger).level.toString().toLowerCase()
            }
        }

        assertEquals("error", FakeService().loggerLevel())
    }

    @Test
    fun `check for given logging level`() {
        class FakeService: AbstractSearchService(loggingLevel = "info") {
            override fun searchResults(searchPhrase: String, page: Int, limit: Int) = SearchServiceResults()

            fun loggerLevel(): String {
                return (logger.underlyingLogger as Logger).level.toString().toLowerCase()
            }
        }

        assertEquals("info", FakeService().loggerLevel())
    }

    @Test
    fun `check rank for existing result`() {
        class FakeService: AbstractSearchService() {
            override fun searchResults(searchPhrase: String, page: Int, limit: Int): SearchServiceResults {
                val resultItem = SearchServiceResultItem("Anything", "http://www.example.com", 1, 1)
                return SearchServiceResults(items = listOf(resultItem))
            }

        }

        assertEquals(1, FakeService().rank("anything", "example.com")!!.rank)
    }

    @Test
    fun `check rank for existing result with empty items`() {
        class FakeService: AbstractSearchService() {
            override fun searchResults(searchPhrase: String, page: Int, limit: Int) =
                SearchServiceResults(items = emptyList())
        }

        assertNull(FakeService().rank("anything", "example.com"))
    }

    @Test
    fun `check rank for empty result`() {
        class FakeService: AbstractSearchService() {
            override fun searchResults(searchPhrase: String, page: Int, limit: Int) = SearchServiceResults()
        }

        assertNull(FakeService().rank("anything", "example.com"))
    }
}
