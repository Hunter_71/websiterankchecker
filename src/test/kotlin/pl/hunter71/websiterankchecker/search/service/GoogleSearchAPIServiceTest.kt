package pl.hunter71.websiterankchecker.search.service

import io.mockk.junit5.MockKExtension
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import pl.hunter71.websiterankchecker.search.service.results.SearchServiceResults


@ExtendWith(MockKExtension::class)
internal class GoogleSearchAPIServiceTest {
    private val emptyResults = SearchServiceResults()

    @Test
    fun `check searchResults for invalid page`() {
        assertEquals(emptyResults, GoogleSearchAPIService.searchResults("anything", -1))
    }

    @Test
    fun `check searchResults for not existing page`() {
        assertEquals(
            emptyResults,
            GoogleSearchAPIService.searchResults("anything", SearchService.MAX_PAGE_NUMBER + 1)
        )
    }
}
