package pl.hunter71.websiterankchecker.search.service

import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Test
import pl.hunter71.websiterankchecker.search.google.GoogleSearchAPI


internal class APIBuilderFactoryTest {

    @Test
    fun `check create Google Search API`() {
        assertNotNull(APIBuilderFactory.create<GoogleSearchAPI>("http://www.example.com/"))
    }
}
