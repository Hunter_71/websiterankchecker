package pl.hunter71.websiterankchecker.search.service.results

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test


internal class SearchServiceResultsTest {
    private val page = 4
    private val item = SearchServiceResultItem("Title", "url", page, 3)

    @Test
    fun `check items`() {
        val results = SearchServiceResults(items = listOf(item))
        assertEquals(1, results.items.size)
        assertEquals(listOf(item), results.items)
    }

    @Test
    fun `check empty items`() {
        assertTrue(SearchServiceResults().items.isEmpty())
    }

    @Test
    fun `check toString for items`() {
        val expected = "Page $page:\n${item.toStringRanked()}"
        assertEquals(expected, SearchServiceResults(items = listOf(item)).toString())
    }

    @Test
    fun `check toString for empty items`() {
        assertEquals("<empty>", SearchServiceResults().toString())
    }
}
