package pl.hunter71.websiterankchecker.search.google

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test


internal class GoogleSearchResultsTest {
    private val resultMetaItem =
        GoogleSearchResultMetaItem("title", "http://www.example.com", "www.example.com")
    private val resultMetaItem2 =
        GoogleSearchResultMetaItem("Title 2", "http://www.example.com", "www.example.com")

    @Test
    fun `check results items`() {
        val results = GoogleSearchResults(items = listOf(resultMetaItem))
        assertEquals(1, results.items!!.size)
        assertEquals(listOf(resultMetaItem), results.items)
    }

    @Test
    fun `check results empty items`() {
        assertTrue(GoogleSearchResults(items = emptyList()).items!!.isEmpty())
    }

    @Test
    fun `check results null items`() {
        assertNull(GoogleSearchResults(items = null).items)
    }

    @Test
    fun `check mapToResponseFields for results`() {
        assertEquals("items(title,link,formattedUrl)", GoogleSearchResults.mapToResponseFields())
    }

    @Test
    fun `check toSearchServiceResult for results`() {
        val results = GoogleSearchResults(items = listOf(resultMetaItem, resultMetaItem2))

        val page = 2
        val searchServiceResults = results.toSearchServiceResult(page)

        val resultsFirstItem = results.items!!.first()
        val searchServiceResultsFirstItem = searchServiceResults.items.first()
        val searchServiceResultsSecondItem = searchServiceResults.items[1]

        assertEquals(resultsFirstItem.title, searchServiceResultsFirstItem.title)
        assertEquals(resultsFirstItem.link, searchServiceResultsFirstItem.url)
        assertEquals(page, searchServiceResultsFirstItem.page)
        assertEquals(1, searchServiceResultsFirstItem.position)
        assertEquals(11, searchServiceResultsFirstItem.rank)

        assertEquals(resultMetaItem2.title, searchServiceResultsSecondItem.title)
        assertEquals(resultMetaItem2.link, searchServiceResultsSecondItem.url)
        assertEquals(page, searchServiceResultsSecondItem.page)
        assertEquals(2, searchServiceResultsSecondItem.position)
        assertEquals(12, searchServiceResultsSecondItem.rank)
    }

    @Test
    fun `check toSearchServiceResult for results empty items`() {
        assertTrue(GoogleSearchResults(items = emptyList()).toSearchServiceResult(1).items.isEmpty())
    }

    @Test
    fun `check toSearchServiceResult for results null items`() {
        assertTrue(GoogleSearchResults(items = null).toSearchServiceResult(1).items.isEmpty())
    }
}
