package pl.hunter71.websiterankchecker.search.google

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test


internal class GoogleSearchResultMetaItemTest {
    private val title = "title"
    private val link = "http://www.example.com"
    private val formattedUrl = "www.example.com"
    private val resultMetaItem = GoogleSearchResultMetaItem(title, link, formattedUrl)

    @Test
    fun `check result meta item`() {
        assertEquals(title, resultMetaItem.title)
        assertEquals(link, resultMetaItem.link)
        assertEquals(formattedUrl, resultMetaItem.formattedUrl)
    }

    @Test
    fun `check mapToResponseFields for result meta item`() {
        assertEquals("title,link,formattedUrl", GoogleSearchResultMetaItem.mapToResponseFields())
    }
}
