plugins {
    kotlin("jvm") version "1.3.72"
    id("com.github.ksoichiro.console.reporter") version "0.6.2"
    id("io.gitlab.arturbosch.detekt") version "1.8.0"
    jacoco
    application
}

repositories {
    mavenCentral()
    jcenter {
        content {
            // just allow to include kotlinx projects
            // detekt needs 'kotlinx-html' for the html report
            includeGroup("org.jetbrains.kotlinx")
        }
    }
}

dependencies {
    implementation(kotlin("stdlib"))
    implementation("com.github.ajalt:clikt:2.7.1")
    implementation("com.natpryce:konfig:1.6.10.0")
    implementation("com.squareup.retrofit2:retrofit:2.3.0")
    implementation("com.squareup.retrofit2:converter-gson:2.3.0")

    // logging
    implementation("ch.qos.logback:logback-classic:1.2.3")
    implementation("io.github.microutils:kotlin-logging:1.7.8")

    testImplementation("org.junit.jupiter:junit-jupiter-api:5.6.2")
    testImplementation("io.mockk:mockk:1.10.0")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:5.4.2")
}

application {
    mainClassName = "pl.hunter71.websiterankchecker.Main"
}

tasks {
    compileKotlin {
        kotlinOptions.jvmTarget = "1.8"
    }
    compileTestKotlin {
        kotlinOptions.jvmTarget = "1.8"
    }

    jar {
        manifest {
            attributes["Main-Class"] = application.mainClassName
        }

        from(
            configurations.runtimeClasspath.get().map {
                if (it.isDirectory) it else zipTree(it)
            }
        )

        // do not build with local configurations
//        processResources.get().exclude("application.*.ini")
    }

    test {
        useJUnitPlatform()
        testLogging {
            events("passed", "skipped", "failed")
        }

        finalizedBy("jacocoTestReport")
    }

    jacoco {
        toolVersion = "0.8.5"
    }

    jacocoTestReport {
        reports {
            xml.isEnabled = false
            csv.isEnabled = false
        }

        finalizedBy("reportCoverage")
    }

    detekt {
        version = "1.8.0"
        config = files("$projectDir/detekt.yaml")
        input = files("$projectDir/src/main/kotlin")
        autoCorrect = true
        failFast = false
        reports {
            xml.enabled = true
            html.enabled = true
        }
    }
}
